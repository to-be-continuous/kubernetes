{
  "name": "Kubernetes",
  "description": "Deploy your application to a [Kubernetes](https://kubernetes.io/) platform using [declarative configuration](https://kubernetes.io/docs/tasks/manage-kubernetes-objects/declarative-config/) or [Kustomize](https://kubernetes.io/docs/tasks/manage-kubernetes-objects/kustomization/)",
  "template_path": "templates/gitlab-ci-k8s.yml",
  "kind": "hosting",
  "prefix": "k8s",
  "is_component": true,
  "variables": [
    {
      "name": "K8S_KUBECTL_IMAGE",
      "description": "The Docker image used to run Kubernetes `kubectl` commands - **set the version required by your Kubernetes server**",
      "default": "registry.hub.docker.com/bitnami/kubectl:latest"
    },
    {
      "name": "K8S_DEFAULT_KUBE_CONFIG",
      "description": "The default kubeconfig to use (either content or file variable) (only define if not using exploded kubeconfig parameters)",
      "secret": true
    },
    {
      "name": "K8S_URL",
      "type": "url",
      "description": "Global Kubernetes API url (only define if using exploded kubeconfig parameters)"
    },
    {
      "name": "K8S_TOKEN",
      "description": "Global Kubernetes API token (only define if using exploded kubeconfig parameters)",
      "secret": true
    },
    {
      "name": "K8S_CA_CERT",
      "description": "Global Kubernetes cluster server certificate authority (only define if using exploded kubeconfig parameters)",
      "secret": true
    },
    {
      "name": "K8S_BASE_APP_NAME",
      "description": "Base application name",
      "default": "$CI_PROJECT_NAME",
      "advanced": true
    },
    {
      "name": "K8S_ENVIRONMENT_URL",
      "type": "url",
      "description": "The default environments url _(only define for static environment URLs declaration)_\n\n_supports late variable expansion (ex: `https://%{environment_name}.k8s.acme.com`)_"
    },
    {
      "name": "K8S_SCRIPTS_DIR",
      "description": "directory where Kubernetes scripts (templates, hook scripts) are located",
      "default": ".",
      "advanced": true
    },
    {
      "name": "K8S_KUSTOMIZE_ENABLED",
      "description": "Set to `true` to enable [Kustomize](https://kubectl.docs.kubernetes.io/references/kustomize/kustomization/)",
      "type": "boolean",
      "advanced": true
    },
    {
      "name": "K8S_KUSTOMIZE_ARGS",
      "description": "Additional [`kubectl kustomize` options](https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands#kustomize)\n\n_For example: `--enable-helm`_",
      "advanced": true
    },
    {
      "name": "K8S_CREATE_NAMESPACE_ENABLED",
      "description": "Set to `true` to enable automatic namespace creation",
      "type": "boolean",
      "advanced": true
    }
  ],
  "features": [
    {
      "id": "kube-score",
      "name": "kube-score",
      "description": "Static code analysis of your Kubernetes templates with [kube-score](https://github.com/zegl/kube-score)",
      "disable_with": "K8S_SCORE_DISABLED",
      "variables": [
        {
          "name": "K8S_KUBE_SCORE_IMAGE",
          "description": "Docker image to run [kube-score](https://github.com/zegl/kube-score)",
          "default": "registry.hub.docker.com/zegl/kube-score:latest"
        },
        {
          "name": "K8S_SCORE_EXTRA_OPTS",
          "description": "Additional [kube-score options](https://github.com/zegl/kube-score#configuration)",
          "advanced": true
        }
      ]
    },
    {
      "id": "review",
      "name": "Review",
      "description": "Dynamic review environments for your topic branches (see GitLab [Review Apps](https://docs.gitlab.com/ee/ci/review_apps/))",
      "variables": [
        {
          "name": "K8S_REVIEW_SPACE",
          "description": "Kubernetes namespace for review env",
          "mandatory": true
        },
        {
          "name": "K8S_REVIEW_APP_NAME",
          "description": "The application name for review env (only define to override default)",
          "advanced": true
        },
        {
          "name": "K8S_REVIEW_AUTOSTOP_DURATION",
          "description": "The amount of time before GitLab will automatically stop `review` environments",
          "default": "4 hours"
        },
        {
          "name": "K8S_REVIEW_ENVIRONMENT_URL",
          "type": "url",
          "description": "The review environments url _(only define for static environment URLs declaration and if different from default)_",
          "advanced": true
        },
        {
          "name": "K8S_REVIEW_KUBE_CONFIG",
          "description": "Specific kubeconfig for review env (only define if not using exploded parameters and if different from global)",
          "secret": true
        },
        {
          "name": "K8S_REVIEW_URL",
          "type": "url",
          "description": "Kubernetes API url for review env (only define if using exploded kubeconfig parameters and if different from global)",
          "advanced": true
        },
        {
          "name": "K8S_REVIEW_TOKEN",
          "description": "Kubernetes API token for review env (only define if using exploded kubeconfig parameters and if different from global)",
          "secret": true
        },
        {
          "name": "K8S_REVIEW_CA_CERT",
          "description": "Kubernetes cluster server certificate authority for review env (only define if using exploded kubeconfig parameters and if different from global)",
          "secret": true
        }
      ]
    },
    {
      "id": "integration",
      "name": "Integration",
      "description": "A continuous-integration environment associated to your integration branch (`develop` by default)",
      "variables": [
        {
          "name": "K8S_INTEG_SPACE",
          "description": "Kubernetes namespace for integration env",
          "mandatory": true
        },
        {
          "name": "K8S_INTEG_APP_NAME",
          "description": "The application name for integration env (only define to override default)",
          "advanced": true
        },
        {
          "name": "K8S_INTEG_ENVIRONMENT_URL",
          "type": "url",
          "description": "The integration environment url _(only define for static environment URLs declaration and if different from default)_",
          "advanced": true
        },
        {
          "name": "K8S_INTEG_KUBE_CONFIG",
          "description": "Specific kubeconfig for integration env (only define if not using exploded parameters and if different from global)",
          "secret": true
        },
        {
          "name": "K8S_INTEG_URL",
          "type": "url",
          "description": "Kubernetes API url for integration env (only define if using exploded kubeconfig parameters and if different from global)",
          "advanced": true
        },
        {
          "name": "K8S_INTEG_TOKEN",
          "description": "Kubernetes API token for integration env (only define if using exploded kubeconfig parameters and if different from global)",
          "secret": true
        },
        {
          "name": "K8S_INTEG_CA_CERT",
          "description": "Kubernetes cluster server certificate authority for integration env (only define if using exploded kubeconfig parameters and if different from global)",
          "secret": true
        }
      ]
    },
    {
      "id": "staging",
      "name": "Staging",
      "description": "An iso-prod environment meant for testing and validation purpose on your production branch (`main` or `master` by default)",
      "variables": [
        {
          "name": "K8S_STAGING_SPACE",
          "description": "Kubernetes namespace for staging env",
          "mandatory": true
        },
        {
          "name": "K8S_STAGING_APP_NAME",
          "description": "The application name for staging env (only define to override default)",
          "advanced": true
        },
        {
          "name": "K8S_STAGING_ENVIRONMENT_URL",
          "type": "url",
          "description": "The staging environment url _(only define for static environment URLs declaration and if different from default)_",
          "advanced": true
        },
        {
          "name": "K8S_STAGING_KUBE_CONFIG",
          "description": "Specific kubeconfig for staging env (only define if not using exploded parameters and if different from global)",
          "secret": true
        },
        {
          "name": "K8S_STAGING_URL",
          "type": "url",
          "description": "Kubernetes API url for staging env (only define if using exploded kubeconfig parameters and if different from global)",
          "advanced": true
        },
        {
          "name": "K8S_STAGING_TOKEN",
          "description": "Kubernetes API token for staging env (only define if using exploded kubeconfig parameters and if different from global)",
          "secret": true
        },
        {
          "name": "K8S_STAGING_CA_CERT",
          "description": "Kubernetes cluster server certificate authority for staging env (only define if using exploded kubeconfig parameters and if  different from global)",
          "secret": true
        }
      ]
    },
    {
      "id": "prod",
      "name": "Production",
      "description": "The production environment",
      "variables": [
        {
          "name": "K8S_PROD_SPACE",
          "description": "Kubernetes namespace for production env",
          "mandatory": true
        },
        {
          "name": "K8S_PROD_APP_NAME",
          "description": "The application name for production env (only define to override default)",
          "advanced": true
        },
        {
          "name": "K8S_PROD_ENVIRONMENT_URL",
          "type": "url",
          "description": "The production environment url _(only define for static environment URLs declaration and if different from default)_",
          "advanced": true
        },
        {
          "name": "K8S_PROD_DEPLOY_STRATEGY",
          "description": "Defines the deployment to production strategy.",
          "type": "enum",
          "values": ["manual", "auto"],
          "default": "manual"
        },
        {
          "name": "K8S_PROD_KUBE_CONFIG",
          "description": "Specific kubeconfig for production env (only define if not using exploded parameters and if different from global)",
          "secret": true
        },
        {
          "name": "K8S_PROD_URL",
          "type": "url",
          "description": "Kubernetes API url for production env (only define if using exploded kubeconfig parameters and if different from global)",
          "advanced": true
        },
        {
          "name": "K8S_PROD_TOKEN",
          "description": "Kubernetes API token for production env (only define if using exploded kubeconfig parameters and if different from global)",
          "secret": true
        },
        {
          "name": "K8S_PROD_CA_CERT",
          "description": "Kubernetes cluster server certificate authority for production env (only define if using exploded kubeconfig parameters and if  different from global)",
          "secret": true
        }
      ]
    }
  ],
  "variants": [
    {
      "id": "vault",
      "name": "Vault",
      "description": "Retrieve secrets from a [Vault](https://www.vaultproject.io/) server",
      "template_path": "templates/gitlab-ci-k8s-vault.yml",
      "variables": [
        {
          "name": "TBC_VAULT_IMAGE",
          "description": "The [Vault Secrets Provider](https://gitlab.com/to-be-continuous/tools/vault-secrets-provider) image to use",
          "default": "registry.gitlab.com/to-be-continuous/tools/vault-secrets-provider:latest",
          "advanced": true
        },
        {
          "name": "VAULT_BASE_URL",
          "description": "The Vault server base API url"
        },
        {
          "name": "VAULT_OIDC_AUD",
          "description": "The `aud` claim for the JWT",
          "default": "$CI_SERVER_URL"
        },
        {
          "name": "VAULT_ROLE_ID",
          "description": "The [AppRole](https://www.vaultproject.io/docs/auth/approle) RoleID",
          "mandatory": true,
          "secret": true
        },
        {
          "name": "VAULT_SECRET_ID",
          "description": "The [AppRole](https://www.vaultproject.io/docs/auth/approle) SecretID",
          "mandatory": true,
          "secret": true
        }
      ]
    },
    {
      "id": "gcp-auth-provider",
      "name": "Google Cloud",
      "description": "This variant uses [Application Default Credentials][gcp-adc] through the `GOOGLE_APPLICATION_CREDENTIALS` variable using Workload Identity federation.",
      "template_path": "templates/gitlab-ci-k8s-gcp.yml",
      "variables": [
        {
          "name": "GCP_OIDC_AUD",
          "description": "The `aud` claim for the JWT token _(only required for [OIDC authentication](https://docs.gitlab.com/ee/ci/cloud_services/aws/))_",
          "default": "$CI_SERVER_URL",
          "advanced": true
        },
        {
          "name": "GCP_OIDC_ACCOUNT",
          "description": "Default Service Account to which impersonate with OpenID Connect authentication"
        },
        {
          "name": "GCP_OIDC_PROVIDER",
          "description": "Default Workload Identity Provider associated with GitLab to [authenticate with OpenID Connect](https://docs.gitlab.com/ee/ci/cloud_services/google_cloud/)"
        },
        {
          "name": "GCP_REVIEW_OIDC_ACCOUNT",
          "description": "Service Account to which impersonate with OpenID Connect authentication on `review` environment",
          "advanced": true
        },
        {
          "name": "GCP_REVIEW_OIDC_PROVIDER",
          "description": "Workload Identity Provider associated with GitLab to [authenticate with OpenID Connect](https://docs.gitlab.com/ee/ci/cloud_services/google_cloud/) on `review` environment",
          "advanced": true
        },
        {
          "name": "GCP_INTEG_OIDC_ACCOUNT",
          "description": "Service Account to which impersonate with OpenID Connect authentication on `integration` environment",
          "advanced": true
        },
        {
          "name": "GCP_INTEG_OIDC_PROVIDER",
          "description": "Workload Identity Provider associated with GitLab to [authenticate with OpenID Connect](https://docs.gitlab.com/ee/ci/cloud_services/google_cloud/) on `integration` environment",
          "advanced": true
        },
        {
          "name": "GCP_STAGING_OIDC_ACCOUNT",
          "description": "Service Account to which impersonate with OpenID Connect authentication on `staging` environment",
          "advanced": true
        },
        {
          "name": "GCP_STAGING_OIDC_PROVIDER",
          "description": "Workload Identity Provider associated with GitLab to [authenticate with OpenID Connect](https://docs.gitlab.com/ee/ci/cloud_services/google_cloud/) on `staging` environment",
          "advanced": true
        },
        {
          "name": "GCP_PROD_OIDC_ACCOUNT",
          "description": "Service Account to which impersonate with OpenID Connect authentication on `production` environment",
          "advanced": true
        },
        {
          "name": "GCP_PROD_OIDC_PROVIDER",
          "description": "Workload Identity Provider associated with GitLab to [authenticate with OpenID Connect](https://docs.gitlab.com/ee/ci/cloud_services/google_cloud/) on `production` environment",
          "advanced": true
        },
        {
          "name": "K8S_KUBECTL_IMAGE",
          "description": "The Docker image used to run Kubernetes `kubectl` commands on [GKE](https://cloud.google.com/kubernetes-engine/docs)",
          "default": "gcr.io/google.com/cloudsdktool/cloud-sdk:latest"
        }
      ]
    },
    {
      "id": "aws-auth-provider",
      "name": "Amazon Web service",
      "description": "This variant uses [OpenID Connect in AWS] to retrieve temporary credentials.",
      "template_path": "templates/gitlab-ci-k8s-aws.yml",
      "variables": [
        {
          "name": "AWS_OIDC_AUD",
          "description": "The `aud` claim for the JWT token _(only required for [OIDC authentication](https://docs.gitlab.com/ee/ci/cloud_services/aws/))_",
          "default": "$CI_SERVER_URL",
          "advanced": true
        },
        {
          "name": "AWS_OIDC_ROLE_ARN",
          "description": "The default role ARN configured",
          "advanced": true
        },
        {
          "name": "AWS_REVIEW_OIDC_ROLE_ARN",
          "description": "The role ARN configured for `review` environment",
          "advanced": true
        },
        {
          "name": "AWS_INTEG_OIDC_ROLE_ARN",
          "description": "The role ARN configured for `integration` environment",
          "advanced": true
        },
        {
          "name": "AWS_STAGING_OIDC_ROLE_ARN",
          "description": "The role ARN configured for `staging` environment",
          "advanced": true
        },
        {
          "name": "AWS_PROD_OIDC_ROLE_ARN",
          "description": "The role ARN configured for `production` environment",
          "advanced": true
        },
        {
          "name": "K8S_KUBECTL_IMAGE",
          "description": "The Docker image used to run Kubernetes `kubectl` commands on [AWS]",
          "default": "docker.io/alpine/k8s:1.32.1"
        }
      ]
    }
  ]
}
