# GitLab CI template for Kubernetes (k8s)

This project implements a GitLab CI/CD template to deploy your application to a [Kubernetes](https://kubernetes.io/) platform 
using [declarative configuration](https://kubernetes.io/docs/tasks/manage-kubernetes-objects/declarative-config/) 
or [Kustomize](https://kubernetes.io/docs/tasks/manage-kubernetes-objects/kustomization/).

## Usage

This template can be used both as a [CI/CD component](https://docs.gitlab.com/ee/ci/components/#use-a-component) 
or using the legacy [`include:project`](https://docs.gitlab.com/ee/ci/yaml/index.html#includeproject) syntax.

### Use as a CI/CD component

Add the following to your `.gitlab-ci.yml`:

```yaml
include:
  # 1: include the component
  - component: $CI_SERVER_FQDN/to-be-continuous/kubernetes/gitlab-ci-k8s@7.3.0
    # 2: set/override component inputs
    inputs:
      # ⚠ this is only an example
      kubectl-image: registry.hub.docker.com/bitnami/kubectl:1.26
      base-app-name: wonderapp
      review-space: myapp-nonprod
      staging-space: myapp-nonprod
      prod-space: myapp-prod
```

### Use as a CI/CD template (legacy)

Add the following to your `.gitlab-ci.yml`:

```yaml
include:
  # 1: include the template
  - project: 'to-be-continuous/kubernetes'
    ref: '7.3.0'
    file: '/templates/gitlab-ci-k8s.yml'

variables:
  # 2: set/override template variables
  # ⚠ this is only an example
  K8S_KUBECTL_IMAGE: registry.hub.docker.com/bitnami/kubectl:1.26
  K8S_BASE_APP_NAME: wonderapp
  K8S_REVIEW_SPACE: myapp-nonprod
  K8S_STAGING_SPACE: myapp-nonprod
  K8S_PROD_SPACE: myapp-prod
```

## Understand

This chapter introduces key notions and principle to understand how this template works.

### Managed deployment environments

This template implements continuous delivery/continuous deployment for projects hosted on Kubernetes platforms.

It allows you to manage automatic deployment & cleanup of standard predefined environments.
Each environment can be enabled/disabled by configuration.
If you're not satisfied with predefined environments and/or their associated Git workflow, you may implement you own environments and
workflow, by reusing/extending the base (hidden) jobs. This is advanced usage and will not be covered by this documentation.

The following chapters present the managed predefined environments and their associated Git workflow.

#### Review environments

The template supports **review** environments: those are dynamic and ephemeral environments to deploy your
_ongoing developments_ (a.k.a. _feature_ or _topic_ branches).

When enabled, it deploys the result from upstream build stages to a dedicated and temporary environment.
It is only active for non-production, non-integration branches.

It is a strict equivalent of GitLab's [Review Apps](https://docs.gitlab.com/ee/ci/review_apps/) feature.

It also comes with a _cleanup_ job (accessible either from the _environments_ page, or from the pipeline view).

#### Integration environment

If you're using a Git Workflow with an integration branch (such as [Gitflow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow)),
the template supports an **integration** environment.

When enabled, it deploys the result from upstream build stages to a dedicated environment.
It is only active for your integration branch (`develop` by default).

#### Production environments

Lastly, the template supports 2 environments associated to your production branch (`master` or `main` by default):

* a **staging** environment (an iso-prod environment meant for testing and validation purpose),
* the **production** environment.

You're free to enable whichever or both, and you can also choose your deployment-to-production policy:

* **continuous deployment**: automatic deployment to production (when the upstream pipeline is successful),
* **continuous delivery**: deployment to production can be triggered manually (when the upstream pipeline is successful).

### Supported authentication methods

The Kubernetes template supports 3 ways of login/accessing your Kubernetes cluster(s):

1. Using [GitLab agents with the CI/CD workflow](https://docs.gitlab.com/ee/user/clusters/agent/ci_cd_workflow.html): when enabled, the template automatically retrieves and uses your Kubernetes cluster configuration (`KUBECONFIG` env),
    :warning: don't forget to set the `KUBE_CONTEXT` variable (to `path/to/agent/project:agent-name`) as [stated in the documentation](https://docs.gitlab.com/ee/user/clusters/agent/ci_cd_workflow.html#environments-that-use-auto-devops).
2. By defining an explicit `kubeconfig` from env (either file or yaml content),
3. By defining explicit `kubeconfig` **exploded parameters** from env: server url, server certificate authority and user token.

### Deployment context variables

In order to manage the various deployment environments, this template provides a couple of **dynamic variables**
that you might use in your hook scripts, deployment manifests and other deployment resources:

* `${environment_type}`: the current deployment environment type (`review`, `integration`, `staging` or `production`)
* `${environment_name}`: a generated application name to use for the current deployment environment (ex: `myproject-review-fix-bug-12` or `myproject-staging`) - _details below_
* `${environment_name_ssc}`: the above application name in [SCREAMING_SNAKE_CASE](https://en.wikipedia.org/wiki/Snake_case) format
    (ex: `MYPROJECT_REVIEW_FIX_BUG_12` or `MYPROJECT_STAGING`)
* `${k8s_namespace}`: the Kubernetes namespace currently used for deployment/cleanup

#### Generated environment name

The `${environment_name}` variable is generated to designate each deployment environment with a unique and meaningful application name.
By construction, it is suitable for inclusion in DNS, URLs, Kubernetes labels...
It is built from:

* the application _base name_ (defaults to `$CI_PROJECT_NAME` but can be overridden globally and/or per deployment environment - _see configuration variables_)
* GitLab predefined `$CI_ENVIRONMENT_SLUG` variable ([sluggified](https://en.wikipedia.org/wiki/Clean_URL#Slug) name, truncated to 24 characters)

The `${environment_name}` variable is then evaluated as:

* `<app base name>` for the production environment
* `<app base name>-$CI_ENVIRONMENT_SLUG` for all other deployment environments
* :bulb: `${environment_name}` can also be overriden per environment with the appropriate configuration variable

Examples (with an application's base name `myapp`):

| `$environment_type` | Branch        | `$CI_ENVIRONMENT_SLUG`  | `$environment_name` |
|---------------------|---------------|-------------------------|---------------------|
| `review`            | `feat/blabla` | `review-feat-bla-xmuzs6`| `myapp-review-feat-bla-xmuzs6` |
| `integration`       | `develop`     | `integration`           | `myapp-integration` |
| `staging`           | `main`        | `staging`               | `myapp-staging` |
| `production`        | `main`        | `production`            | `myapp` |

### Supported deployment methods

The Kubernetes template supports three techniques to deploy your code:

1. script-based deployment,
2. template-based deployment using raw Kubernetes manifests (with [variables substitution](#variables-substitution-mechanism)),
3. template-based deployment using [Kustomization files](https://kubectl.docs.kubernetes.io/references/kustomize/kustomization/) (with [variables substitution](#variables-substitution-mechanism)).

#### 1: script-based deployment

In this mode, you only have to provide a shell script that fully implements the deployment using the [`kubectl` CLI](https://kubernetes.io/docs/reference/kubectl/overview/).

The deployment script is searched as follows:

1. look for a specific `k8s-deploy-$environment_type.sh` in the `$K8S_SCRIPTS_DIR` directory in your project (e.g. `k8s-deploy-staging.sh` for staging environment),
2. if not found: look for a default `k8s-deploy.sh` in the `$K8S_SCRIPTS_DIR` directory in your project,
3. if not found: the GitLab CI template assumes you're using the template-based deployment policy.

:warning: `k8s-deploy-$environment_type.sh` or `k8s-deploy.sh` needs to be executable, you can add flag execution with:  `git update-index --chmod=+x k8s-deploy.sh`

#### 2: template-based deployment

In this mode, you have to provide a [Kubernetes deployment file](https://kubernetes.io/docs/concepts/workloads/controllers/deployment)
in your project structure, and let the GitLab CI template [`kubectl apply`](https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands#apply) it.

The template processes the following steps:

1. _optionally_ executes the `k8s-pre-apply.sh` script in your project to perform specific environment pre-initialization (for e.g. create required services),
2. looks for your Kubernetes deployment file, performs [variables substitution](#variables-substitution-mechanism) and [`kubectl apply`](https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands#apply) it,
    1. look for a specific `deployment-$environment_type.yml` in your project (e.g. `deployment-staging.yml` for staging environment),
    2. fallbacks to default `deployment.yml`.
3. _optionally_ executes the `k8s-post-apply.sh` script in your project to perform specific environment post-initialization stuff,

:warning: `k8s-pre-apply.sh` or `k8s-post-apply.sh` needs to be executable, you can add flag execution with:  `git update-index --chmod=+x k8s-pre-apply.sh`

#### 3: Kustomize-based deployment

In this mode, you have to provide a [Kustomization file](https://kubectl.docs.kubernetes.io/references/kustomize/kustomization/)
in your project structure, and let the template [`kubectl apply`](https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands#apply) it.

The template processes the following steps:

1. _optionally_ executes the `k8s-pre-apply.sh` script in your project to perform specific environment pre-initialization (for e.g. create required services),
2. looks for your Kustomization file, performs [variables substitution](#variables-substitution-mechanism), generates the manifests with [`kubectl kustomize`](https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands#kustomize) and [`kubectl apply`](https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands#apply) it,
    1. looks for an environment-specific [overlay](https://kubectl.docs.kubernetes.io/references/kustomize/glossary/#overlay) file `./$environment_type/kustomization.yaml` (e.g. `./staging/kustomization.yaml ` for staging environment),
    2. fallbacks to default `kustomization.yaml`.
3. _optionally_ executes the `k8s-post-apply.sh` script in your project to perform specific environment post-initialization stuff,

:warning: `k8s-pre-apply.sh` or `k8s-post-apply.sh` needs to be executable, you can add flag execution with:  `git update-index --chmod=+x k8s-pre-apply.sh`
#### Readiness script

After deployment (either script-based or template-based), the GitLab CI template _optionally_ executes the `k8s-readiness-check.sh` hook script to wait & check for the application to be ready (if not found, the template assumes the application was successfully started).

:warning: `k8s-readiness-check.sh` needs to be executable, you can add flag execution with:  `git update-index --chmod=+x k8s-pre-apply.sh`

### Supported cleanup methods

The Kubernetes template supports three techniques to destroy an environment (actually only review environments):

1. script-based deployment,
2. template-based deployment using raw Kubernetes manifests (with [variables substitution](#variables-substitution-mechanism)),
3. template-based deployment using [Kustomization files](https://kubectl.docs.kubernetes.io/references/kustomize/kustomization/).

#### 1: script-based cleanup

In this mode, you only have to provide a shell script that fully implements the environment cleanup using the [`kubectl` CLI](https://kubernetes.io/docs/reference/kubectl/overview/).

The a deployment script is searched as follows:

1. look for a specific `k8s-cleanup-$environment_type.sh` in the `$K8S_SCRIPTS_DIR` directory in your project (e.g. `k8s-cleanup-staging.sh` for staging environment),
2. if not found: look for a default `k8s-cleanup.sh` in the `$K8S_SCRIPTS_DIR` directory in your project,
3. if not found: the GitLab CI template assumes you're using the template-based cleanup policy.

:warning: `k8s-cleanup-$environment_type.sh` or `k8s-cleanup.sh` needs to be executable, you can add flag execution with:  `git update-index --chmod=+x k8s-cleanup.sh`

> TIP: a nice way to implement environment cleanup is to declare the label `app=${environment_name}` on every Kubernetes
> object associated to your environment.
> Then environment cleanup can be implemented very easily with command `kubectl delete all,pvc,secret,ingress -l "app=${environment_name}"`

#### 2: template-based cleanup

In this mode, you mainly let Kubernetes delete all objects from your Kubernetes deployment file.

The template processes the following steps:

1. _optionally_ executes the `k8s-pre-cleanup.sh` script in your project to perform specific environment pre-cleanup stuff,
2. looks for your Kubernetes deployment file, performs [variables substitution](#variables-substitution-mechanism) and [`kubectl delete`](https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands#apply) it,
    1. look for a specific `deployment-$environment_type.yml` in your project (e.g. `deployment-staging.yml` for staging environment),
    2. fallbacks to default `deployment.yml`.
3. _optionally_ executes the `k8s-post-cleanup.sh` script in your project to perform specific environment post-cleanup (for e.g. delete bound services).

:warning: `k8s-pre-cleanup.sh` or `k8s-post-cleanup.sh` needs to be executable, you can add flag execution with:  `git update-index --chmod=+x k8s-pre-cleanup.sh`

#### 3: Kustomize-based cleanup

In this mode, you mainly let Kubernetes delete all objects from your [Kustomization file(s)](https://kubectl.docs.kubernetes.io/references/kustomize/kustomization/).

The template processes the following steps:

1. _optionally_ executes the `k8s-pre-cleanup.sh` script in your project to perform specific environment pre-cleanup stuff,
2. looks for your Kustomization file, performs variables substitution and [`kubectl delete`](https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands#apply) it,
    1. looks for an environment-specific [overlay](https://kubectl.docs.kubernetes.io/references/kustomize/glossary/#overlay) file `./$environment_type/kustomization.yaml` (e.g. `./staging/kustomization.yaml ` for staging environment),
    2. fallbacks to default `kustomization.yaml`.
3. _optionally_ executes the `k8s-post-cleanup.sh` script in your project to perform specific environment post-cleanup (for e.g. delete bound services).

:warning: `k8s-pre-cleanup.sh` or `k8s-post-cleanup.sh` needs to be executable, you can add flag execution with:  `git update-index --chmod=+x k8s-pre-cleanup.sh`

#### Cleanup job limitations

When using this template, you have to be aware of one limitation (bug) with the cleanup job.

By default, the cleanup job triggered automatically on branch deletion will **fail** due to not being able
to fetch the Git branch prior to executing the job (obviously because the branch was just deleted).
This is pretty annoying, but as you may see above, deleting an env _may_ require scripts from the project...

In a future version, the template will rely on [Kustomize](https://kustomize.io/) and will be able to delete an
entire environment using the `app` label. In the meantime we're just sorry about this bug, but for now there is not
much we can do:

* remind to delete your review env **manually before deleting the branch**
* otherwise you'll have to do it afterwards from your computer (using `kubectl` CLI) or from the some k8s web console.

### Using variables

You have to be aware that your deployment (and cleanup) scripts have to be able to cope with various environments, each 
with different application names, exposed routes, settings, ...
Part of this complexity can be handled by the lookup policies described above (ex: one script/manifest per env) and also 
by using available environment variables:

1. [deployment context variables](#deployment-context-variables) provided by the template:
    * `${environment_type}`: the current environment type (`review`, `integration`, `staging` or `production`)
    * `${environment_name}`: the application name to use for the current environment (ex: `myproject-review-fix-bug-12` or `myproject-staging`)
    * `${environment_name_ssc}`: the application name in [SCREAMING_SNAKE_CASE](https://en.wikipedia.org/wiki/Snake_case) format
       (ex: `MYPROJECT_REVIEW_FIX_BUG_12` or `MYPROJECT_STAGING`)
    * `${k8s_namespace}`: the Kubernetes namespace currently used for deployment/cleanup
    * `${hostname}`: the environment hostname, extracted from the current environment url (after late variable expansion - see below)
2. any [GitLab CI variable](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)
3. any [custom variable](https://docs.gitlab.com/ee/ci/variables/#for-a-project)
    (ex: `${SECRET_TOKEN}` that you have set in your project CI/CD variables)

#### Variables substitution mechanism

While your scripts may freely use any of the available variables, your Kubernetes and Kustomize
resources can use a **variables substitution** mechanism implemented by the template:

- Using the syntax `${VARIABLE_NAME}` or `%{VARIABLE_NAME}`.\
  :warning: Curly braces (`{}`) are mandatory in the expression (`$VARIABLE_NAME` won't be processed).
- Each of those expressions will be **dynamically expanded** in your resource files with the variable value, right before being used.
- Variable substitution expressions **must be contained in double-quoted strings**.
  The substitution implementation takes care of escaping characters that need to be (double quote `"`, backslash `\`, tab `\t`, carriage return `\n` and line feed `\r`).
- Variable substitution can be prevented by appending `# nosubst` at the end of any line.\
  Ex:
  ```yaml
  apiVersion: v1
  kind: ConfigMap
  metadata:
    # ${environment_name} will be expanded
    labels:
      app: "${environment_name}"
    name: "${environment_name}"
  data:
    application.yml: |
      remote:
        some-service:
            name: '${REMOTE_SERVICE_NAME}' # nosubst
  ```

### Environments URL management

The K8S template supports two ways of providing your environments url:

* a **static way**: when the environments url can be determined in advance, probably because you're exposing your routes through a DNS you manage,
* a [**dynamic way**](https://docs.gitlab.com/ee/ci/environments/#set-a-dynamic-environment-url): when the url cannot be known before the
  deployment job is executed.

The **static way** can be implemented simply by setting the appropriate configuration variable(s) depending on the environment (see environments configuration chapters):

* `$K8S_ENVIRONMENT_URL` to define a default url pattern for all your envs,
* `$K8S_REVIEW_ENVIRONMENT_URL`, `$K8S_INTEG_ENVIRONMENT_URL`, `$K8S_STAGING_ENVIRONMENT_URL` and `$K8S_PROD_ENVIRONMENT_URL` to override the default.

> :information_source: Each of those variables support a **late variable expansion mechanism** with the `%{somevar}` syntax, 
> allowing you to use any dynamically evaluated variables such as `${environment_name}`.
>
> Example:
>
> ```yaml
> variables:
>   K8S_BASE_APP_NAME: "wonderapp"
>   # global url for all environments
>   K8S_ENVIRONMENT_URL: "https://%{environment_name}.nonprod.acme.domain"
>   # override for prod (late expansion of $K8S_BASE_APP_NAME not needed here)
>   K8S_PROD_ENVIRONMENT_URL: "https://$K8S_BASE_APP_NAME.acme.domain"
>   # override for review (using separate resource paths)
>   K8S_REVIEW_ENVIRONMENT_URL: "https://wonderapp-review.nonprod.acme.domain/%{environment_name}"
> ```

To implement the **dynamic way**, your deployment script shall simply generate a `environment_url.txt` file in the working directory, containing only
the dynamically generated url. When detected by the template, it will use it as the newly deployed environment url.

### Deployment output variables

Each deployment job produces _output variables_ that are propagated to downstream jobs (using [dotenv artifacts](https://docs.gitlab.com/ee/ci/yaml/artifacts_reports.html#artifactsreportsdotenv)):

* `$environment_type`: set to the type of environment (`review`, `integration`, `staging` or `production`),
* `$environment_name`: the application name (see below),
* `$environment_url`: set to the environment URL (whether determined statically or dynamically).

Those variables may be freely used in downstream jobs (for instance to run acceptance tests against the latest deployed environment).

You may also add and propagate your own custom variables, by pushing them to the `kubernetes.env` file in your [deployment script or hook](#supported-deployment-methods).

## Configuration reference

### Secrets management

Here are some advices about your **secrets** (variables marked with a :lock:):

1. Manage them as [project or group CI/CD variables](https://docs.gitlab.com/ee/ci/variables/#for-a-project):
    * [**masked**](https://docs.gitlab.com/ee/ci/variables/#mask-a-cicd-variable) to prevent them from being inadvertently
      displayed in your job logs,
    * [**protected**](https://docs.gitlab.com/ee/ci/variables/#protected-cicd-variables) if you want to secure some secrets
      you don't want everyone in the project to have access to (for instance production secrets).
2. In case a secret contains [characters that prevent it from being masked](https://docs.gitlab.com/ee/ci/variables/#mask-a-cicd-variable),
  simply define its value as the [Base64](https://en.wikipedia.org/wiki/Base64) encoded value prefixed with `@b64@`:
  it will then be possible to mask it and the template will automatically decode it prior to using it.
3. Don't forget to escape special characters (ex: `$` -> `$$`).

### Global configuration

The Kubernetes template uses some global configuration used throughout all jobs.

| Input / Variable | Description                                                                                                                                                             | Default value                                                                                          |
| --------------------- | -------------------------------------- | ----------------- |
| `kubectl-image` / `K8S_KUBECTL_IMAGE` | the Docker image used to run Kubernetes `kubectl` commands <br/>:warning: **set the version required by your Kubernetes server**                                        | `registry.hub.docker.com/bitnami/kubectl:latest`                                                       <br/>[![Trivy Badge](https://to-be-continuous.gitlab.io/doc/secu/trivy-badge-K8S_KUBECTL_IMAGE.svg)](https://to-be-continuous.gitlab.io/doc/secu/trivy-K8S_KUBECTL_IMAGE) |
| `base-app-name` / `K8S_BASE_APP_NAME` | Default application name                                                                                                                                                | `$CI_PROJECT_NAME` ([see GitLab doc](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)) |
| `environment-url` / `K8S_ENVIRONMENT_URL`    | Default environments url _(only define for static environment URLs declaration)_<br/>_supports late variable expansion (ex: `https://%{environment_name}.k8s.acme.com`)_ | _none_                                                                                                 |
| `KUBE_CONTEXT`      | Defines the context to be used in `KUBECONFIG`. When using [GitLab agents with the CI/CD workflow](https://docs.gitlab.com/ee/user/clusters/agent/ci_cd_workflow.html), the value should be like `path/to/agent/project:agent-name`. To use different agents per environment, define an [environment-scoped CI/CD variable](https://docs.gitlab.com/ee/ci/environments/index.html#limit-the-environment-scope-of-a-cicd-variable) for each agent. | _none_ |
| :lock: `K8S_DEFAULT_KUBE_CONFIG`| The default kubeconfig to use (either content or file variable)                                                                                                         | **required if not using exploded kubeconfig parameters**                                               |
| `url` / `K8S_URL` | the Kubernetes API url                                                                                                                                                  | **required if using exploded kubeconfig parameters**                                                   |
| :lock: `K8S_CA_CERT`          | the default Kubernetes server certificate authority                                                                                                                     | **optional if using exploded kubeconfig parameters**                                                   |
| :lock: `K8S_TOKEN`     | Default service account token                                                                                                                                           | **required if using exploded kubeconfig parameters**                                                   |
| `scripts-dir` / `K8S_SCRIPTS_DIR` | directory where k8s scripts (hook scripts) are located                                                                                                                  | `.` _(root project dir)_                                                                               |
| `kustomize-enabled` / `K8S_KUSTOMIZE_ENABLED` | Set to `true` to force using [Kustomize](https://kubectl.docs.kubernetes.io/references/kustomize/kustomization/)                                                        | _none_ (disabled)                                                                                      |
| `kustomize-args` / `K8S_KUSTOMIZE_ARGS` | Additional [`kubectl kustomize` options](https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands#kustomize)<br/>_for example: `--enable-helm`_ | _none_                                                                                               |
| `create-namespace` / `K8S_CREATE_NAMESPACE_ENABLED` | Set to `true` to enable automatic namespace creation  | `false`                                                                                               |
| `DOCKER_CONTAINER_STABLE_IMAGE` | Docker image name to use for staging/prod                                                                                                                               | **has to be defined when not chaining execution from Docker template**                                 |
| `DOCKER_CONTAINER_UNSTABLE_IMAGE` | Docker image name to use for review                                                                                                                                     | **has to be defined when not chaining execution from Docker template**                                 |

### Review environments configuration

Review environments are dynamic and ephemeral environments to deploy your _ongoing developments_ (a.k.a. _feature_ or
_topic_ branches).

They are **disabled by default** and can be enabled by setting the `K8S_REVIEW_SPACE` variable (see below).

Here are variables supported to configure review environments:

| Input / Variable | Description                            | Default value     |
| ------------------------ | -------------------------------------- | ----------------- |
| `review-space` / `K8S_REVIEW_SPACE` | k8s namespace for `review` env         | _none_ (disabled) |
| `review-app-name` / `K8S_REVIEW_APP_NAME` | application name for `review` env      | `"${K8S_BASE_APP_NAME}-${CI_ENVIRONMENT_SLUG}"` |
| `review-environment-url` / `K8S_REVIEW_ENVIRONMENT_URL`| The review environments url _(only define for static environment URLs declaration and if different from default)_ | `$K8S_ENVIRONMENT_URL` |
| :lock: `K8S_REVIEW_KUBE_CONFIG` | Specific kubeconfig for `review` env _(only define if not using exploded kubeconfig parameters and if different from default)_ | `$K8S_DEFAULT_KUBE_CONFIG` |
| `review-url` / `K8S_REVIEW_URL` | Kubernetes API url for `review` env  _(only define if using exploded kubeconfig parameters and if different from default)_    | `$K8S_URL` |
| :lock: `K8S_REVIEW_CA_CERT`     | the Kubernetes server certificate authority for `review` env _(only define if using exploded kubeconfig parameters and if different from default)_ | `$K8S_CA_CERT` |
| :lock: `K8S_REVIEW_TOKEN`| service account token for `review` env _(only define if using exploded kubeconfig parameters and if different from default)_ | `$K8S_TOKEN` |
| `review-autostop-duration` / `K8S_REVIEW_AUTOSTOP_DURATION` | The amount of time before GitLab will automatically stop `review` environments | `4 hours` |

### Integration environment configuration

The integration environment is the environment associated to your integration branch (`develop` by default).

It is **disabled by default** and can be enabled by setting the `K8S_INTEG_SPACE` variable (see below).

Here are variables supported to configure the integration environment:

| Input / Variable | Description                            | Default value     |
| ------------------------ | -------------------------------------- | ----------------- |
| `integ-space` / `K8S_INTEG_SPACE` | k8s namespace for `integration` env    | _none_ (disabled) |
| `integ-app-name` / `K8S_INTEG_APP_NAME` | application name for `integration` env | `$K8S_BASE_APP_NAME-integration` |
| `integ-environment-url` / `K8S_INTEG_ENVIRONMENT_URL`| The integration environment url _(only define for static environment URLs declaration and if different from default)_ | `$K8S_ENVIRONMENT_URL` |
| :lock: `K8S_INTEG_KUBE_CONFIG` | Specific kubeconfig for `integration` env _(only define if not using exploded kubeconfig parameters and if different from default)_ | `$K8S_DEFAULT_KUBE_CONFIG` |
| `integ-url` / `K8S_INTEG_URL` | Kubernetes API url for `integration` env  _(only define if using exploded kubeconfig parameters and if different from default)_ | `$K8S_URL` |
| :lock: `K8S_INTEG_CA_CERT`      | the Kubernetes server certificate authority for `integration` env _(only define if using exploded kubeconfig parameters and if different from default)_ | `$K8S_CA_CERT` |
| :lock: `K8S_INTEG_TOKEN` | service account token for `integration` env _(only define if using exploded kubeconfig parameters and if different from default)_ | `$K8S_TOKEN` |

### Staging environment configuration

The staging environment is an iso-prod environment meant for testing and validation purpose associated to your production branch (`main` or `master` by default).

It is **disabled by default** and can be enabled by setting the `K8S_STAGING_SPACE` variable (see below).

Here are variables supported to configure the staging environment:

| Input / Variable | Description                            | Default value     |
| ------------------------ | -------------------------------------- | ----------------- |
| `staging-space` / `K8S_STAGING_SPACE` | k8s namespace for `staging` env        | _none_ (disabled) |
| `staging-app-name` / `K8S_STAGING_APP_NAME` | application name for `staging` env     | `$K8S_BASE_APP_NAME-staging` |
| `staging-environment-url` / `K8S_STAGING_ENVIRONMENT_URL`| The staging environment url _(only define for static environment URLs declaration and if different from default)_ | `$K8S_ENVIRONMENT_URL` |
| :lock: `K8S_STAGING_KUBE_CONFIG` | Specific kubeconfig for `staging` env _(only define if not using exploded kubeconfig parameters and if different from default)_ | `$K8S_DEFAULT_KUBE_CONFIG` |
| `staging-url` / `K8S_STAGING_URL` | Kubernetes API url for `staging` env  _(only define if using exploded kubeconfig parameters and if different from default)_   | `$K8S_URL` |
| :lock: `K8S_STAGING_CA_CERT`    | the Kubernetes server certificate authority for `staging` env _(only define if using exploded kubeconfig parameters and if different from default)_ | `$K8S_CA_CERT` |
| :lock: `K8S_STAGING_TOKEN`| service account token for `staging` env _(only define if using exploded kubeconfig parameters and if different from default)_ | `$K8S_TOKEN` |

### Production environment configuration

The production environment is the final deployment environment associated with your production branch (`main` or `master` by default).

It is **disabled by default** and can be enabled by setting the `K8S_PROD_SPACE` variable (see below).

Here are variables supported to configure the production environment:

| Input / Variable | Description                            | Default value     |
| ------------------------ | -------------------------------------- | ----------------- |
| `prod-space` / `K8S_PROD_SPACE` | k8s namespace for `production` env     | _none_ (disabled) |
| `prod-app-name` / `K8S_PROD_APP_NAME` | application name for `production` env  | `$K8S_BASE_APP_NAME` |
| `prod-environment-url` / `K8S_PROD_ENVIRONMENT_URL`| The production environment url _(only define for static environment URLs declaration and if different from default)_ | `$K8S_ENVIRONMENT_URL` |
| :lock: `K8S_PROD_KUBE_CONFIG` | Specific kubeconfig for `production` env _(only define if not using exploded kubeconfig parameters and if different from default)_ | `$K8S_DEFAULT_KUBE_CONFIG` |
| `prod-url` / `K8S_PROD_URL` | Kubernetes API url for `production` env  _(only define if using exploded kubeconfig parameters and if different from default)_| `$K8S_URL` |
| :lock: `K8S_PROD_CA_CERT`       | the Kubernetes server certificate authority for `production` env _(only define if using exploded kubeconfig parameters and if different from default)_ | `$K8S_CA_CERT` |
| :lock: `K8S_PROD_TOKEN`  | service account token for `production` env _(only define if using exploded kubeconfig parameters and if different from default)_ | `$K8S_TOKEN` |
| `prod-deploy-strategy` / `K8S_PROD_DEPLOY_STRATEGY` | Defines the deployment to production strategy. One of `manual` (i.e. _one-click_) or `auto`. | `manual` |

### kube-score job

The Kubernetes template enables [kube-score](https://github.com/zegl/kube-score) analysis of your Kubernetes object definitions.
This job is mapped to the `test` stage and is **active** by default.

Here are its parameters:

| Input / Variable | Description                                                          | Default value     |
| ---------------------- | -------------------------------------------------------------------- | ----------------- |
| `kube-score-image` / `K8S_KUBE_SCORE_IMAGE` | Docker image to run [kube-score](https://github.com/zegl/kube-score) | `registry.hub.docker.com/zegl/kube-score:latest` **it is recommended to set a tool version compatible with your Kubernetes cluster** <br/>[![Trivy Badge](https://to-be-continuous.gitlab.io/doc/secu/trivy-badge-K8S_KUBE_SCORE_IMAGE.svg)](https://to-be-continuous.gitlab.io/doc/secu/trivy-K8S_KUBE_SCORE_IMAGE) |
| `score-disabled` / `K8S_SCORE_DISABLED` | Set to `true` to disable the `kube-score` analysis                             | _none_ (enabled) |
| `score-extra-opts` / `K8S_SCORE_EXTRA_OPTS` | [Additional options](https://github.com/zegl/kube-score#configuration) to `kube-score` command line | _none_ |

## Variants

### Vault variant

This variant allows delegating your secrets management to a [Vault](https://www.vaultproject.io/) server.

#### :warning: Change default `K8S_KUBECTL_IMAGE`

The Vault variant requires `curl` or `wget` to retrieve secrets from the Vault server, which makes it incompatible with the
[default `K8S_KUBECTL_IMAGE` from Bitnami](https://bitnami.com/stack/kubectl/containers), as `curl` and `wget` are no longer part of it.

As a result, when using the Vault variant, you'll have to select a `K8S_KUBECTL_IMAGE` that - in addition to `kubectl` - contains `curl` or `wget`.
For instance [container-oc](https://github.com/appuio/container-oc) images (see example below).

#### Configuration

In order to be able to communicate with the Vault server, the variant requires the additional configuration parameters:

| Input / Variable | Description                            | Default value     |
| ----------------- | -------------------------------------- | ----------------- |
| `TBC_VAULT_IMAGE` | The [Vault Secrets Provider](https://gitlab.com/to-be-continuous/tools/vault-secrets-provider) image to use (can be overridden) | `registry.gitlab.com/to-be-continuous/tools/vault-secrets-provider:latest` |
| `vault-base-url` / `VAULT_BASE_URL` | The Vault server base API url          | **must be defined** |
| `vault-oidc-aud` / `VAULT_OIDC_AUD` | The `aud` claim for the JWT | `$CI_SERVER_URL` |
| :lock: `VAULT_ROLE_ID`   | The [AppRole](https://www.vaultproject.io/docs/auth/approle) RoleID | _none_ |
| :lock: `VAULT_SECRET_ID` | The [AppRole](https://www.vaultproject.io/docs/auth/approle) SecretID | _none_ |

By default, the variant will authentifacte using a [JWT ID token](https://docs.gitlab.com/ee/ci/secrets/id_token_authentication.html). To use [AppRole](https://www.vaultproject.io/docs/auth/approle) instead the `VAULT_ROLE_ID` and `VAULT_SECRET_ID` should be defined as secret project variables.

#### Usage

Then you may retrieve any of your secret(s) from Vault using the following syntax:

```text
@url@http://vault-secrets-provider/api/secrets/{secret_path}?field={field}
```

With:

| Parameter                        | Description                            |
| -------------------------------- | -------------------------------------- |
| `secret_path` (_path parameter_) | this is your secret location in the Vault server |
| `field` (_query parameter_)      | parameter to access a single basic field from the secret JSON payload |

#### Example

```yaml
include:
  # main template
  - component: $CI_SERVER_FQDN/to-be-continuous/kubernetes/gitlab-ci-k8s@7.3.0
    inputs:
      # ⚠ oc-container image (includes required curl)
      kubectl-image: registry.hub.docker.com/docker.io/appuio/oc:v4.14
  # Vault variant
  - component: $CI_SERVER_FQDN/to-be-continuous/kubernetes/gitlab-ci-k8s-vault@7.3.0
    inputs:
      # audience claim for JWT
      vault-oidc-aud: "https://vault.acme.host"
      vault-base-url: "https://vault.acme.host/v1"

variables:
    # Secrets managed by Vault
    K8S_DEFAULT_KUBE_CONFIG: "@url@http://vault-secrets-provider/api/secrets/b7ecb6ebabc231/my-app/kubernetes/noprod?field=kube_config"
    K8S_PROD_KUBE_CONFIG: "@url@http://vault-secrets-provider/api/secrets/b7ecb6ebabc231/my-app/kubernetes/prod?field=kube_config"
```

### Google Cloud variant

This variant uses [Application Default Credentials][gcp-adc] through the `GOOGLE_APPLICATION_CREDENTIALS` variable using Workload Identity federation.

List of requirements before using this variant:

1. You must have a Workload Identity Federation Pool and Provider configured,
2. You must have a Service Account with the `roles/iam.workloadIdentityUser` IAM role
   granted to the Workload Identity [principal][gcp-iam-principals] matching your Gitlab project or group,
3. Optionally, you can set the `GOOGLE_CLOUD_PROJECT` template variable
   to define the default Google Cloud project.
4. You must have create a `kubeconfig.yaml` configuration which [enable application default credentials for kubectl](https://cloud.google.com/kubernetes-engine/docs/how-to/api-server-authentication#environments-without-gcloud)

The Gitlab documentation has some [details about Workload Identity Federation integration][gcp-gitlab-wif].

This [blog post about OIDC impersonation through Workload Identify Federation][gcp-wif-example] might also be of help.

[gcp-adc]: https://cloud.google.com/docs/authentication/client-libraries
[gcp-provider]: https://registry.terraform.io/providers/hashicorp/google/latest/docs/guides/provider_reference#running-terraform-outside-of-google-cloud
[gcp-iam-principals]: https://cloud.google.com/iam/docs/principal-identifiers
[gcp-gitlab-wif]: https://docs.gitlab.com/ee/ci/cloud_services/google_cloud/
[gcp-wif-example]: https://blog.salrashid.dev/articles/2021/understanding_workload_identity_federation/#oidc-impersonated

#### Configuration

The variant requires the additional configuration parameters:

| Input / Variable  | Description                            | Default value     |
| ----------------- | -------------------------------------- | ----------------- |
| `gcp-oidc-aud` / `GCP_OIDC_AUD` | The `aud` claim for the JWT token      | `$CI_SERVER_URL` |
| `gcp-oidc-provider` / `GCP_OIDC_PROVIDER` | Default Workload Identity Provider associated with GitLab to [authenticate with OpenID Connect](https://docs.gitlab.com/ee/ci/cloud_services/google_cloud/) | _none_ |
| `gcp-oidc-account` / `GCP_OIDC_ACCOUNT` | Default Service Account to which impersonate with OpenID Connect authentication | _none_ |
| `gcp-review-oidc-provider` / `GCP_REVIEW_OIDC_PROVIDER` | Workload Identity Provider associated with GitLab to [authenticate with OpenID Connect](https://docs.gitlab.com/ee/ci/cloud_services/google_cloud/) on `review` environment _(only define to override default)_ | _none_ |
| `gcp-review-oidc-account` / `GCP_REVIEW_OIDC_ACCOUNT` | Service Account to which impersonate with OpenID Connect authentication on `review` environment _(only define to override default)_ | _none_ |
| `gcp-integ-oidc-provider` / `GCP_INTEG_OIDC_PROVIDER` | Workload Identity Provider associated with GitLab to [authenticate with OpenID Connect](https://docs.gitlab.com/ee/ci/cloud_services/google_cloud/) on `integration` environment _(only define to override default)_ | _none_ |
| `gcp-integ-oidc-account` / `GCP_INTEG_OIDC_ACCOUNT` | Service Account to which impersonate with OpenID Connect authentication on `integration` environment _(only define to override default)_ | _none_ |
| `gcp-staging-oidc-provider` / `GCP_STAGING_OIDC_PROVIDER` | Workload Identity Provider associated with GitLab to [authenticate with OpenID Connect](https://docs.gitlab.com/ee/ci/cloud_services/google_cloud/) on `staging` environment _(only define to override default)_ | _none_ |
| `gcp-staging-oidc-account` / `GCP_STAGING_OIDC_ACCOUNT` | Service Account to which impersonate with OpenID Connect authentication on `staging` environment _(only define to override default)_ | _none_ |
| `gcp-prod-oidc-provider` / `GCP_PROD_OIDC_PROVIDER` | Workload Identity Provider associated with GitLab to [authenticate with OpenID Connect](https://docs.gitlab.com/ee/ci/cloud_services/google_cloud/) on `production` environment _(only define to override default)_ | _none_ |
| `gcp-prod-oidc-account` / `GCP_PROD_OIDC_ACCOUNT` | Service Account to which impersonate with OpenID Connect authentication on `production` environment _(only define to override default)_ | _none_ |
| `kubectl-image` / `K8S_KUBECTL_IMAGE` | The Docker image used to run Kubernetes `kubectl` commands on [GKE](https://cloud.google.com/kubernetes-engine/docs) | `gcr.io/google.com/cloudsdktool/cloud-sdk:latest` |

#### Example

With a common default `GCP_OIDC_PROVIDER` and `GCP_OIDC_ACCOUNT` configuration for non-prod environments, and a specific one for production:

```yaml
include:
  # main template
  - component: $CI_SERVER_FQDN/to-be-continuous/kubernetes/gitlab-ci-k8s@7.3.0
  # Google Cloud variant
  - component: $CI_SERVER_FQDN/to-be-continuous/kubernetes/gitlab-ci-k8ss-gcp@7.3.0
    inputs:
      # common OIDC config for non-prod envs
      gcp-oidc-provider: "projects/<gcp_nonprod_proj_id>/locations/global/workloadIdentityPools/<pool_id>/providers/<provider_id>"
      gcp-oidc-account: "<name>@$<gcp_nonprod_proj_id>.iam.gserviceaccount.com"
      # specific OIDC config for prod
      gcp-prod-oidc-provider: "projects/<gcp_prod_proj_id>/locations/global/workloadIdentityPools/<pool_id>/providers/<provider_id>"
      gcp-prod-oidc-account: "<name>@$<gcp_prod_proj_id>.iam.gserviceaccount.com"
```

### Amazon Web service variant

This variant use the OIDC and [AWS STS](https://docs.aws.amazon.com/fr_fr/STS/latest/APIReference/welcome.html) in AWS to get credential

#### Prerequesite

- [Create an OpenID Connect (OIDC) identity provider in IAM
  ](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_roles_providers_create_oidc.html)
- [Configure a web identity role](https://docs.gitlab.com/ee/ci/cloud_services/aws/#configure-a-role-and-trust)

#### Configuration

The  image from alpine `k8s` is required for the use of aws-iam-authenticator.
  
The variant requires the additional configuration parameters :

| Input / Variable                                          | Description                                                                                                                                                                            | Default value    |
|-----------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------------|
| `aws-oidc-aud` / `AWS_OIDC_AUD`                           | The `aud` claim for the JWT token                                                                                                                                                      | `$CI_SERVER_URL` |
| `aws-oidc-role-arn` / `AWS_OIDC_ROLE_ARN`                 | Default IAM Role ARN associated with GitLab to [authenticate using OpenID Connect](https://docs.gitlab.com/ee/ci/cloud_services/aws/)                                                  | _none_           |
| `aws-review-oidc-role-arn` / `AWS_REVIEW_OIDC_ROLE_ARN`   | IAM Role ARN associated with GitLab to [authenticate using OpenID Connect](https://docs.gitlab.com/ee/ci/cloud_services/aws/) on `review` env _(only define to override default)_      | _none_           |
| `aws-integ-oidc-role-arn` / `AWS_INTEG_OIDC_ROLE_ARN`     | IAM Role ARN associated with GitLab to [authenticate using OpenID Connect](https://docs.gitlab.com/ee/ci/cloud_services/aws/) on `integration` env _(only define to override default)_ | _none_           |
| `aws-staging-oidc-role-arn` / `AWS_STAGING_OIDC_ROLE_ARN` | IAM Role ARN associated with GitLab to [authenticate using OpenID Connect](https://docs.gitlab.com/ee/ci/cloud_services/aws/) on `staging` env _(only define to override default)_     | _none_           |
| `aws-prod-oidc-role-arn` / `AWS_PROD_OIDC_ROLE_ARN`       | IAM Role ARN associated with GitLab to [authenticate using OpenID Connect](https://docs.gitlab.com/ee/ci/cloud_services/aws/) on `production` env _(only define to override default)_  | _none_           |
| `kubectl-image` / `K8S_KUBECTL_IMAGE`                     | The Docker image used to run Kubernetes `kubectl` commands on [AWS]                                                                                                                    | `docker.io/alpine/k8s:1.32.1` |

#### Example

With a common default `AWS_OIDC_ROLE_ARN`  configuration for non-prod environments, and a specific one for production:

```yaml
include:
  # main template
  - component: $CI_SERVER_FQDN/to-be-continuous/kubernetes/gitlab-ci-k8s@7.3.0
  # AWS variant
  - component: $CI_SERVER_FQDN/to-be-continuous/kubernetes/gitlab-ci-k8s-aws@7.3.0
    inputs:
      # common OIDC config for non-prod envs
      aws-oidc-role-arn: "arn:aws:iam::<project_id>:role/<role_name>"
      # specific OIDC config for prod
      aws-prod-oidc-role-arn: "arn:aws:iam::<project_id>:role/<role_name>"
```